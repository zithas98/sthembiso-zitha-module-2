void main() {
  var appOfTheYear = winningApp2021("Ambani Africa", 2021, 'Best');
  appOfTheYear.nameOfApp = "Ambani Africa";
  appOfTheYear.year = 2021;
  appOfTheYear.category[0] = "Best Gaming Solution";
  appOfTheYear.category[1] = "Best Educational Solution";
  appOfTheYear.category[2] = "Best 'South African' Solution";
  appOfTheYear.category[3] = "Overall App of the year";
  appOfTheYear.display();
}

class winningApp2021 {
  String nameOfApp = ""; // Declaration of the name of the app
  List<String> category = [
    "Best Gaming Solution",
    "Best Educational Solution",
    "Best 'South African' Solution ",
    "Overall App of the year"
  ];
  int year = 0;

  winningApp2021(String nameOfApp, int year, String categry) {
    this.nameOfApp = nameOfApp;
    this.year = year;
    this.category = category;
  }

  void display() {
    print("Name of the app is ${this.nameOfApp}"); // Print of Name of the app
    print(
        "$nameOfApp won ${this.category[0]}, ${this.category[1]}, ${this.category[2]}, ${category[3]} "); //Print of Categories the app won in
    print("$nameOfApp won in the year ${this.year}"); //Print of the year it won
    print(nameOfApp.toUpperCase()); //Print of the Name of App into UPPERCASE
  }
}
