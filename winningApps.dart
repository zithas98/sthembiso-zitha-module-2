import 'package:flutter/cupertino.dart';

void main() {
  List<String> winningApp = [
    // Array of winning apps since 2012
    "FNB",
    "Plascon",
    "Rapid Target",
    "Bookly",
    "Dstv",
    "Nedbank",
    "Price Check",
    "Supersport",
    "Dstv Now",
    "M4 Jam",
    "Hear ZA",
    "ikhokha",
    "Tuta Me",
    "Order In",
    "Pick n Pay",
    "Awethu Project",
    "Zulzi",
    "Shyft",
    "Eco Slips",
    "Besmarta",
    "Vula",
    "My Pregnancy Journey",
    "Sixty",
    "Technishen",
    "Guardian Health",
    "Takealot"
  ];

  winningApp.sort(((a, b) {
    // Sorting the list alphabetically
    return a.compareTo(b);
  }));

  print("The sorted list is: \n$winningApp"); // Printing out sorted list
  int totalApps = winningApp.length;

  print(
      "The winning app of 2017 is Shyft"); // Printing out the winning app of 2017

  print(
      "The winning app of 2017 is Khula"); // Printing out the winning app of 2018

  print(
      "Total number of apps $totalApps"); // Printing out the total number of apps
}
